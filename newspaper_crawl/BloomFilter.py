#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 16 18:57:30 2017

@author: Samuele Garda
"""
"""
Bloom filter extesion for Scrapy.
"""

import os
from scrapy.dupefilters import BaseDupeFilter
from scrapy.utils.job import job_dir
from scrapy.utils.request import request_fingerprint
from pybloom import ScalableBloomFilter


class BLOOMDupeFilter(BaseDupeFilter):
  """
  BloomFilter for duplicates filtering in Scrapy.
  """
    

  def __init__(self, path=None, initial_capacity=None, error_rate=None, mode=ScalableBloomFilter.SMALL_SET_GROWTH):
      self.file = None
      self.fingerprints = None
  
      if path:
          self.file = os.path.join(path, ".BloomFilterDumps")
  
          if os.path.exists(self.file):
              with open(self.file, "rb") as rf:
                  self.fingerprints = ScalableBloomFilter.fromfile(rf)
  
      if not self.fingerprints:
          self.fingerprints = ScalableBloomFilter(initial_capacity, error_rate, mode)
  
  @classmethod
  def from_settings(cls, settings):
      p = settings.get("BLOOMFILTER_PATH",job_dir(settings))
      ic = settings.get("BLOOMFILTER_SIZE", 512)
      ert = settings.get("BLOOMFILTER_ERROR_RATE", 0.0001)
  
      return cls(path=p, initial_capacity=ic, error_rate=ert)
  
  def request_seen(self, request):
      # do something or filter rule with request.url
      fp = self.request_fingerprint(request)
      if fp in self.fingerprints:
          return True
      self.fingerprints.add(fp)
  
  @staticmethod
  def request_fingerprint(request):
      return request_fingerprint(request)
  
  def close(self, reason):
      if self.file:
          with open(self.file, "wb") as wf:
            self.fingerprints.tofile(wf)