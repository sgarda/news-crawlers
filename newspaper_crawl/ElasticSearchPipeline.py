"""
Simplified version of python ScrapyElasticsearch module.
For complete version see : https://github.com/knockrentals/scrapy-elasticsearch
Modified because the `id` of scrapy items was not set properly before being sent to the ES index.
"""

from datetime import datetime
from elasticsearch import Elasticsearch, helpers

import logging
import types


class InvalidSettingsException(Exception):
    pass


class ElasticSearchPipeline(object):
    settings = None
    es = None
    items_buffer = []

    @classmethod
    def validate_settings(cls, settings):
        def validate_setting(setting_key):
            if settings[setting_key] is None:
                raise InvalidSettingsException('%s is not defined in settings.py' % setting_key)

        required_settings = {'ELASTICSEARCH_INDEX', 'ELASTICSEARCH_TYPE'}

        for required_setting in required_settings:
            validate_setting(required_setting)

    @classmethod
    def from_crawler(cls, crawler):
        ext = cls()
        ext.settings = crawler.settings

        cls.validate_settings(ext.settings)

        es_servers = ext.settings['ELASTICSEARCH_SERVERS']
        es_servers = es_servers if isinstance(es_servers, list) else [es_servers]
        # for local testing
#        ext.es = Elasticsearch(hosts = ['localhost'],timeout=ext.settings.get('ELASTICSEARCH_TIMEOUT', 60))

		# on server
        ext.es = Elasticsearch(hosts=es_servers, timeout=ext.settings.get('ELASTICSEARCH_TIMEOUT', 60))
                               
                               #verify_certs=False,use_ssl = True, http_auth= "admin:admin")
        return ext

    def index_item(self, item):

        index_name = self.settings['ELASTICSEARCH_INDEX']
        index_suffix_format = self.settings.get('ELASTICSEARCH_INDEX_DATE_FORMAT', None)

        if index_suffix_format:
            index_name += "-" + datetime.strftime(datetime.now(),index_suffix_format)

        index_action = {
            '_index': index_name,
            '_type': self.settings['ELASTICSEARCH_TYPE'],
            '_id' : item['url'],
            '_source': dict(item)
        }


        self.items_buffer.append(index_action)

        if len(self.items_buffer) >= self.settings.get('ELASTICSEARCH_BUFFER_LENGTH', 500):
            self.send_items()
            self.items_buffer = []
            logging.info('{0} to Elastic Search {1}'.format(self.settings['ELASTICSEARCH_BUFFER_LENGTH'],
                         self.settings['ELASTICSEARCH_INDEX']) )

    def send_items(self):
        helpers.bulk(self.es, self.items_buffer)

    def process_item(self, item, spider):
        if isinstance(item, types.GeneratorType) or isinstance(item, list):
            for each in item:
                self.process_item(each, spider)
        else:
            self.index_item(item)
            logging.debug('Item sent to Elastic Search %s' % self.settings['ELASTICSEARCH_INDEX'])
            return item

    def close_spider(self, spider):
        if len(self.items_buffer):
            self.send_items()

