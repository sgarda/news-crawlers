# #!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue May  9 14:57:39 2017

@author: Samuele Garda
"""

from scrapy import Item,Field


class ArticleItem(Item):
    # define the fields for your item here like:
    # name = scrapy.Field()

    url = Field()
    title = Field()
    body = Field()
    subtitle = Field()
    related_links = Field()
    author = Field()
    article_tag = Field()
    list_of_tags = Field()
    keywords = Field()
    news_keywords = Field()
    publication_date = Field()
    caption = Field()
    category = Field()
    premium = Field()
    taxonomy = Field()
    opinion = Field()
