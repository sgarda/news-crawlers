# #!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  9 14:57:39 2017

@author: Samuele Garda
"""
# Scrapy settings for newspaper_crawl project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#     http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html
#     http://scrapy.readthedocs.org/en/latest/topics/spider-middleware.html
#


BOT_NAME = 'newspaper_crawl'

SPIDER_MODULES = ['newspaper_crawl.spiders']
NEWSPIDER_MODULE = 'newspaper_crawl.spiders'

ITEM_PIPELINES = {
    'newspaper_crawl.CleanArticlePipeline.CleanArticlePipeline' : 100,
    'newspaper_crawl.JsonExportPipeline.JsonExportPipeline' : 200,
    'newspaper_crawl.ElasticSearchPipeline.ElasticSearchPipeline' : 300,

}

#SPIDER_MIDDLEWARES = {
#    'scrapy_deltafetch.DeltaFetch': 100,
#}

#EXTENSIONS = {
#    'newspaper_crawl.DumpStatsExtension.DumpStatsExtension': 100,
#}

#####################
### GENERAL SETTINGS
####################

USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36'
ROBOTSTXT_OBEY = True
#DEPTH_LIMIT = 0
CONCURRENT_REQUESTS = 3
DOWNLOAD_DELAY = 1
CONCURRENT_REQUESTS_PER_DOMAIN = 3
CONCURRENT_REQUESTS_PER_IP = 3
COOKIES_ENABLED = False

####################
### STORE DOWNLOAD
####################

DOWNLOAD_DIRECTORY = '/data/websci/samuele.garda'
# Standard way of scrapy to store items
#FEED_URI = '/home/ele/Scrivania/%(name)s_%(category)s.json'
#FEED_FORMAT= 'jsonlines' 

########################
### PERSISTENT CRAWLING
########################

#DUPEFILTER_DEBUG = True
DUPEFILTER_CLASS = 'scrapy.dupefilters.RFPDupeFilter' # 'newspaper_crawl.BloomFilter.BLOOMDupeFilter'
## Folder where fingerprints for duplicate filters are savad !!!unique for each spider!!!

## OVERRIDEN IN SPIDER
#JOBDIR = '/home/ele/Scrivania/HPI/data/websci/samuele.garda'

###############
# ES SETTINGS
###############

ELASTICSEARCH_SERVERS = ['localhost']
ELASTICSEARCH_INDEX = 'newspaper_test' ## OVERRIDEN IN `run_spider.py`
ELASTICSEARCH_TYPE = 'news_article'

#ELASTICSEARCH_INDEX_DATE_FORMAT - the format for date suffix for the index, see python datetime.strftime for format. Default is no date suffix.
#ELASTICSEARCH_UNIQ_KEY - optional field, unique key in string (must be a field declared in model, see items.py)
#ELASTICSEARCH_BUFFER_LENGTH - optional field, number of items to be processed during each bulk insertion to Elasticsearch. Default size is 500.
#ELASTICSEARCH_AUTH  - optional field, set to 'NTLM' to use NTLM authentification
#ELASTICSEARCH_USERNAME - optional field, set to 'DOMAIN\username', only used with NLTM authentification
#ELASTICSEARCH_PASSWORD - optional field, set to your 'password', only used with NLTM authentification





##############
### LOGGING
##############

LOG_LEVEL = 'INFO' 
#LOG_FILE = '/home/ele/Scrivania/test/Guardian/guardian.log' #OVERRIDEN IN SPIDER
#DUMP_STATS_INTERVAL = 360

####################
### DELTA FETCH
####################
# Scrapy pluging for persistent crawling, ignoring already visited pages. 
# Create a database of fingerprints in DELTAFETCH_DIR.
# See here https://blog.scrapinghub.com/2016/07/20/scrapy-tips-from-the-pros-july-2016/ 
# for customizing `deltafetch_key` (default = url fingerprint) 
# ??Could overlap with dupefilter?? 

#DELTAFETCH_ENABLED = True
#DELTAFETCH_DIR = '/home/ele/Scrivania/test/Guardian'
#DELTAFETCH_RESET = False


##########################
### BLOOMFILTER SETTINGS
#########################
#BLOOMFILTER_PATH
#BLOOMFILTER_SIZE = 20000
#BLOOMFILTER_ERROR_RATE = 0.00001





