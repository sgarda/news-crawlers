  #!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 16 16:17:32 2017

@author: Samuele Garda
"""

import os
import re
from datetime import datetime
from scrapy import signals
from scrapy.utils.python import to_bytes
from scrapy.exporters import BaseItemExporter
from scrapy.utils.serialize import ScrapyJSONEncoder



class JsonExporter(BaseItemExporter):
  """
  JSON item exporter
  """
  
  def __init__(self,**kwargs):
    """
    Construct new JSON exporter
    """
    self._configure(kwargs, dont_fail=True)
    kwargs.setdefault('ensure_ascii', not self.encoding)
    self.encoder = ScrapyJSONEncoder(**kwargs)
  
  def export_item(self, item):
    """
    Serialiaze Scrapy Item object in dictionary and encode it.
    
    :param item: item to be serialiazed
    :type item: scrapy.Item
    """
    itemdict = dict(self._get_serialized_fields(item))
    data = self.encoder.encode(itemdict) + '\n'
    return data
    
class JsonExportPipeline(object):
  """
  Scrapy pipeline to export item retrieved into JSON files.
  """
  
  def __init__(self, path,**kwargs):
    self.path = path 
    self.files = {}
    self.exporter = JsonExporter()
    self.curr_file = None

  @classmethod
  def from_crawler(cls, crawler):
    pipeline = cls(path = crawler.settings.get('DOWNLOAD_DIRECTORY')) 
    crawler.signals.connect(pipeline.spider_closed, signals.spider_closed)
    return pipeline
  
  def process_item(self, item, spider):
    
    try:
      year = re.findall('\d{4}',datetime.strftime(item['publication_date'].date(),'%Y-%m-%d'))[0]
    except IndexError:
      year = 'yearnotfound'
   
    try:
      if item['category'] and len(item['category']) > 0:
        category = item['category']
    except KeyError:
      category = 'seednotfound'
          
    if hasattr(spider,'seed_path'):
      self.curr_file = os.path.join(self.path,spider.name, spider.seed_path,year)
      file = open('{}.json'.format(self.curr_file),'a+b')
      self.files[spider.name+'_'+spider.seed_path] = file
        
    else:
      if not os.path.exists(os.path.join(self.path,spider.name,category)):
        os.mkdir(os.path.join(self.path,spider.name,category))
      self.curr_file = os.path.join(self.path,spider.name,category,year)
      file = open('{0}.json'.format(self.curr_file),'a+b')        
      self.files[spider.name] = file
      
    data = self.exporter.export_item(item)
    file.write(to_bytes(data, self.exporter.encoding))
    spider.logger.debug("Item {0} wrote correctly in {1}".format(item['url'],file)) 

    return item
  
  def spider_closed(self, spider):
    
    if hasattr(spider, 'seed_path'):
      try:
        dict_key = spider.name+'_'+spider.seed_path
        file = self.files.pop(dict_key)
        file.close()
      except KeyError:
        spider.logger.warning("`{}` not found! Maybe no data retrieved!".format(self.curr_file))
    else:
      try:
        file_name = spider.name
        file = self.files.pop(file_name)
        file.close()
      except KeyError:
        spider.logger.warning("`{}` not found! Maybe no data retrieved!".format(self.curr_file))
        
    
      
    
    
    
   
  
        
  
      
              
            
          
          
          
        
        
        