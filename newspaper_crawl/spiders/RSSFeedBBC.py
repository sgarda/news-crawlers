# -*- coding: utf-8 -*-
"""
Created on Mon Aug 4  16:39:42 2017

@author: Samuele Garda
"""

from scrapy.link import Link
from newspaper_crawl.items import ArticleItem
from scrapy.loader import ItemLoader
from scrapy.loader.processors import Compose, Join, TakeFirst
from newspaper_crawl.spiders.AbstractSpiders.RSSFeedSpider import RSSFeedSpider


class RSSFeedBBCSpider(RSSFeedSpider):
    """
    Class that implements the RSSFeedSpider for the BBC news.
 
    """
  
    name = 'RSSFeedBBC'
    allowed_domains = ['www.bbc.co.uk']
    start_urls = 'http://feeds.bbci.co.uk/news'
    url_suffix = 'rss.xml'

    def get_links(self,response):
      """
      Retrieve links from `ìndex page` (e.g. links present in /domain/seed/page=32).
      :return: list (of Link objects)
      """
      
      raw_links = response.xpath('//item/link/text()').extract()
      
      links = [Link(raw_link) for raw_link in raw_links if 'newsbeat' not in raw_link.split('/')]
      
      return links
    
    def parse_item(self,response):
      
      """
      Spider method that do the actual parsing for the relevant urls.
      """   
      self.logger.info('Scraping extracetd link: %s',response.url)
                
      b = ItemLoader(item=ArticleItem(), response = response)
      
      b.default_input_processor = Join(separator='') 
      b.default_output_processor = Compose(lambda x : x[0])
            
      b.add_value('url', response.url)
      
      b.add_xpath('title','//title//text()')
      
      b.add_xpath('subtitle','//meta[@name="description"]/@content')
      
      b.add_xpath('body','//div[@property="articleBody"]/descendant-or-self::*[not(self::span or self::style or self::p[@class="story-body__introduction"] or self::script)]/text()[normalize-space()]')
      
      b.add_xpath('publication_date','//div[@class="date date--v2"]/text()[1]',TakeFirst())
      
      b.add_value('category', self.seed)
      
      
      b.add_xpath('author','//meta[@property="article:author"]/@content', Join(',')) # ??bbcnews on facebook??
        
#      b.add_xpath('keywords','//meta[@name="keywords"]/@content')
#      
#      b.add_xpath('news_keywords','//meta[@name="news_keywords"]/@content')
#      
#      b.add_xpath('article_tag','//meta[@property="article:tag"]/@content')
#      
#      b.add_xpath('list_of_tags','//a[@class="list-of-tags__item-link"]')
#      
#      b.add_xpath('premium','//meta[@name="tmg.premium.state"]/@content')
#      
      b.add_xpath('taxonomy', '//meta[@property="article:section"]/@content')
#      
#      b.add_value('opinion', response.url , re = 'commentisfree')
      
      return b.load_item()
      
      
