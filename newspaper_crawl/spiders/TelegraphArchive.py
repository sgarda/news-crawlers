# -*- coding: utf-8 -*-
from scrapy.linkextractors.lxmlhtml import LxmlLinkExtractor
from newspaper_crawl.items import ArticleItem
from scrapy.loader import ItemLoader
from scrapy.loader.processors import Join,Compose, MapCompose
from newspaper_crawl.spiders.AbstractSpiders.ArchiveSpider import ArchiveSpider

class TelegraphArchiveSpider(ArchiveSpider):
  """
  Class that implements the ArchiveSpider for Telegraph news archive.
  """
  name = "TelegraphArchive"
  allowed_domains = ["www.telegraph.co.uk"]
  start_urls = 'http://www.telegraph.co.uk'
  url_suffix = '.html'
  
  
  def get_links(self,response):
    """
    Retrieve links from `ìndex page` (e.g. links present in /domain/archive/2017-01-01)
    :return: list (of Link objects)
    """
    links = LxmlLinkExtractor(allow=[r'/news/worldnews/.*',r'/news/politics/.*',r'/news/uknews/.*', 
                                     r'/news/economics/.*',r'/news/newstopics/.*'],
                                     unique = True).extract_links(response)
    return links
      
  def parse_item(self, response):
    """
    Spider method that do the actual parsing for the relevant urls.
    """
    
    self.logger.info('Scraping extracetd link: %s',response.url)
      
    t = ItemLoader(item=ArticleItem(), response=response)
    
    t.default_input_processor = Join(separator=' ')
    t.default_output_processor = Compose(lambda x : x[0])
        
    t.add_value('url',response.url)
    
    t.add_xpath('title', '//title//text()')
    
    t.add_xpath('subtitle', '//h2[@itemprop="alternativeHeadline description"]//text()',
                MapCompose(str.strip))
    
    t.add_xpath('body', '//div[@id="mainBodyArea"]/descendant-or-self::*[not(self::script or self::style)]/text()[normalize-space()]')
    
    t.add_xpath('publication_date', '//meta[@name="DCSext.articleFirstPublished"]/@content', 
                re = '\d{4}.\d{2}.\d{2}.\d{2}.\d{2}')
    
    t.add_value('category', response.url.split('/')[4])
                
    t.add_xpath('author', '//meta[@name="DCSext.author"]/@content',Join(',')) 
    
    t.add_xpath('keywords', '//meta[@name="keywords"]/@content') 
    
    t.add_xpath('news_keywords', '//meta[@name="news_keywords"]/@content')
    
    
    t.add_xpath('article_tag', '//meta[@property="article:tag"]/@content') 
    
    t.add_xpath('list_of_tags', '//a[@class="list-of-tags__item-link"]')
    
    t.add_xpath('premium','//meta[@name="tmg.premium.state"]/@content')
    
    t.add_value('taxonomy', response.url, re = '/news/|/news/uk/|/news/politics/|/education/|/scotland/| \
                /business/|/money/|/environment/')
    
#    t.add_value('opinion', response.url , re = 'commentisfree')
   
    return t.load_item()
  