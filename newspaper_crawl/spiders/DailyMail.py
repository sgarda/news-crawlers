# -*- coding: utf-8 -*-
"""
Created on Mon Jul 17 16:39:42 2017

@author: Samuele Garda
"""

import os
import sys
import re
from datetime import datetime
from scrapy.http import Request
from scrapy.spiders import CrawlSpider
from scrapy.exceptions import CloseSpider
from scrapy.linkextractors.lxmlhtml import LxmlLinkExtractor
from newspaper_crawl.items import ArticleItem
from scrapy.loader import ItemLoader
from scrapy.loader.processors import Compose,Join,TakeFirst,MapCompose
from scrapy import signals
from scrapy.spidermiddlewares.httperror import HttpError
from twisted.internet.error import DNSLookupError,TimeoutError,TCPTimedOutError,ConnectionRefusedError,NoRouteError




class DailyMailSpider(CrawlSpider):
  """
  Class that implements the Spider.
  Implemented parameters of the Spider `category`. 
  Parameter must be given as input in the command line preceded by `-a` or in the `run_spiders.py`
  script in the `process.Crawl` function.
  """
  name = "DailyMail"    
  allowed_domains = ["www.dailymail.co.uk"]
  
  def __init__(self, path = '', seed='politics', daily = False, stop = None, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.seed = seed
    self.daily = daily
    self.stop = stop
    self.seed_path = seed.replace('/','_') if len(seed.split('/')) > 1 else seed
    self.path = path
    self.topics = set()
    self.__error = None
    
  @classmethod
  def from_crawler(cls, crawler, *args, **kwargs):
    spider = super(DailyMailSpider, cls).from_crawler(crawler, *args, **kwargs)
    crawler.signals.connect(spider.spider_opened, signal=signals.spider_opened)
    crawler.signals.connect(spider.spider_closed, signal=signals.spider_closed)
    return spider
  
  def spider_opened(self,spider):
    
    if self.daily == False:
      try:
        self.__error = open('{0}_{1}_{2}.err'.format(os.path.join(self.path,self.name,self.seed_path,self.name), \
                           self.seed_path,datetime.strftime(datetime.now(),"%d-%m-%Y_%H:%M:%S")),'w+')    
      except IOError:
        self.logger.error('You need to create the download folder for the spider and the category before!')
    else:
      pass

    
  def spider_closed(self,spider):
    if self.daily == False:
      self.__error.close()
      self.logger.info('\n\nRETRIVED TOPICS : %s\n' %self.topics)
    else:
      pass

  def get_links(self,response):
    """
    Retrieve links from `ìndex page` (e.g. links present in /domain/seed/page=32).
    :return: list (of Link objects)
    """
    
    links = LxmlLinkExtractor(allow=[], 
                                  restrict_xpaths = ['//h2[@class="sch-res-section cleared link-wocc"]'],
                                  unique = True).extract_links(response)
  
    return links
  
  
  def all_stop_dates(self,links):
    """
    Method not really implemented. Not found a way to extract stop dates from url
    """
    pass
  
  def start_requests(self):
    """
    Spider method to initialize starting requests. In this case range of pages with step = 50.
    It stops the spider when the range is over.
    """
    if self.daily == False:
      for i in range(0,sys.maxsize,50):
        yield Request(url = 'http://www.dailymail.co.uk/home/search.html?offset={0}&size=50&sel=site&searchPhrase={1}&sort=recent&channel=news&type=article&days=all'.format(i,self.seed),
                      callback = self.parse_index,errback = self.errback_parse,dont_filter = True)
    else:
       yield Request(url = 'http://www.dailymail.co.uk/home/search.html?offset=0&size=50&sel=site&searchPhrase={0}&sort=recent&channel=news&type=article&days=today'.format(self.seed),
                      callback = self.parse_index,errback = self.errback_parse,dont_filter = True)
      
    
  def parse_index(self,response):
    """
    Method of the Spider to extract all relevant urls from the index pages.
    It stops the Spider when first page without links to follow is encountered.
    """
    self.logger.info('Visiting index page : %s',response.url)
    
    new_topics = response.xpath('//div[@class="searchnavigator cleared"]/h4[contains(text(),"Topic")]/following-sibling::ul \
                                //text()[normalize-space()]').extract()
    new_topics = set([re.sub(r'\([^)]*\)', '',topic) for topic in new_topics])
    self.topics.update(new_topics)
    
    links = LxmlLinkExtractor(allow=[], 
                                  restrict_xpaths = ['//h2[@class="sch-res-section cleared link-wocc"]'],
                                  unique = True).extract_links(response)
    if not links:
      raise CloseSpider('No more link to follow')
    else:
      for link in links:
         yield Request(url = link.url, callback = self.parse_item, errback = self.errback_parse)
    
        
  def parse_item(self,response):
    
    self.logger.info('Scraping extracetd link: %s',response.url)
    
    d = ItemLoader(item=ArticleItem(), response=response)
    
    d.default_input_processor = Join(separator=' ')
    d.default_output_processor = Compose(lambda x : x[0])
    
    d.add_value('url',response.url)
    
    d.add_xpath('title', '//div[@id="js-article-text"]/h1/text()')
    
    d.add_xpath('subtitle', '//meta[@name="description"]/@content')
    
    d.add_xpath('body','//div[@itemprop="articleBody"]//p//text()[normalize-space()]',Join(separator=''))
    
    d.add_xpath('publication_date','//meta[@property="article:published_time"]/@content', TakeFirst(),
                re = '\d{4}.\d{2}.\d{2}.\d{2}.\d{2}')
    
    d.add_value('category', self.seed)
#                
#    # taking 2 fields !! second one is correct one
    d.add_xpath('author', '//meta[@property= "article:author"][2]/@content', MapCompose(lambda x : x.replace(',','')))
    
    d.add_xpath('keywords', '//meta[@name="keywords"]/@content') 
    
    d.add_xpath('news_keywords', '//meta[@name="news_keywords"]/@content')
    
    d.add_xpath('article_tag', '//meta[@property="article:tag"]/@content', Join(',')) 
    
#    d.add_xpath('list_of_tags', '//a[@class="list-of-tags__link"]//text()') 
    
#    d.add_xpath('premium','//meta[@name="tmg.premium.state"]/@content')
    
    d.add_value('taxonomy', response.url, re = '/news/')
    
#    d.add_value('opinion', response.url , re = 'commentisfree')
   
    return d.load_item()
          
  def errback_parse(self,failure):
    """
    Spider method to catch error in requested pages.
    If Spider runs in `daily mode` error and relative url appear in stdout.
    Otherwise are stored in an `error file`.
    """
    
    def print_err_msg(err_msg):
      if self.daily == True:
        self.logger.error(err_msg)
      else:
        self.__error.write(err_msg)
 
    if failure.check(HttpError):
      response = failure.value.response
      err_msg = 'HttpError on\t{0}\n'.format(response.url)
      print_err_msg(err_msg)
      

    elif failure.check(DNSLookupError):
      request = failure.request
      err_msg = 'DNSLookupError on\t{0}\n'.format(request.url)
      print_err_msg(err_msg)
      
    elif failure.check(TimeoutError, TCPTimedOutError):
      request = failure.request
      err_msg = 'TimeoutError on\t{0}\n'.format(request.url)
      print_err_msg(err_msg)
 
      
    elif failure.check(ConnectionRefusedError):
      request = failure.request
      err_msg = 'ConnectionRefusedError on\t{0}\n'.format(request.url)
      print_err_msg(err_msg)

    elif failure.check(NoRouteError):
      request = failure.request
      err_msg = 'NoRouteError on\t{0}\n'.format(request.url)
      print_err_msg(err_msg)