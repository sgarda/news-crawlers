#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  4 16:12:49 2017

@author: Samuele Garda
"""

from abc import ABCMeta,abstractmethod
from scrapy.spiders import CrawlSpider
from scrapy.http import Request
from scrapy.spidermiddlewares.httperror import HttpError
from twisted.internet.error import DNSLookupError,TimeoutError,TCPTimedOutError,ConnectionRefusedError,NoRouteError


class RSSFeedSpider(CrawlSpider,metaclass=ABCMeta):
  """
  Abstract class for Spiders that crawl RSSFeed pages. It can run only in `daily mode`.
  
  """
  
  url_suffix = str
  
  def __init__(self,path = '', seed = None, *args, **kwargs):
    """
    Construct new Spider for rss feed.
    
    :param path: where to store error file and downloaded file (same as in `../JsonExportPipeline.py`)
    :type path: string
    :param seed: seed of the subdomain to crawl
    :type seed: string
    
    :rytpe: scrapy.spiders.CrawlSpider
    """
    super().__init__(*args, **kwargs)
    self.path = path
    self.seed = seed
    self.__error = None
    
  def start_requests(self):
    """
    Spider method to initialize starting requests. In this case a single one.

    """
   
    yield Request(url = '{0}/{1}/{2}'.format(self.start_urls,self.seed,self.url_suffix),
                      callback = self.parse_index, errback = self.errback_parse,dont_filter = True)
    
  @abstractmethod 
  def get_links(self,response):
    """
    Method that extract links from the `index page`.
    It needs to be defined by actual Spidder class in order for this one to be instantiated. 
    """
    pass 
      
      
  def parse_index(self, response):
    """
    Method of the Spider to extract all relevant urls from the index page.
    """
    self.logger.info('Visiting index page : %s',response.url)
    
    links = self.get_links(response)
    
    if links:
      for link in links:
        yield Request(url = link.url, callback = self.parse_item, errback = self.errback_parse)
    else:
      self.logger.info("No link found in %s" %response.url)
  
  @abstractmethod 
  def parse_item(self,response):
    """
    Method that performs the actual scraping of the retrieved links in the `index page`.
    It needs to be defined by actual Spidder class in order for this one to be instantiated. 
    """
    
  def errback_parse(self,failure):
    """
    Spider method to catch error in requested pages. 
    Since it can be run only for a daily crawl, errors and relative url appear in stdout
    """
    
    def stdout_err_msg(err_msg):
      self.logger.error(err_msg)
    
    if failure.check(HttpError):
      response = failure.value.response
      err_msg = 'HttpError on\t{0}\n'.format(response.url)
      stdout_err_msg(err_msg)
      

    elif failure.check(DNSLookupError):
      request = failure.request
      err_msg = 'DNSLookupError on\t{0}\n'.format(request.url)
      stdout_err_msg(err_msg)
      
    elif failure.check(TimeoutError, TCPTimedOutError):
      request = failure.request
      err_msg = 'TimeoutError on\t{0}\n'.format(request.url)
      stdout_err_msg(err_msg)
 
      
    elif failure.check(ConnectionRefusedError):
      request = failure.request
      err_msg = 'ConnectionRefusedError on\t{0}\n'.format(request.url)
      stdout_err_msg(err_msg)

    elif failure.check(NoRouteError):
      request = failure.request
      err_msg = 'NoRouteError on\t{0}\n'.format(request.url)
      stdout_err_msg(err_msg)