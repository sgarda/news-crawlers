import os
import sys
from abc import ABCMeta,abstractmethod
from scrapy.spiders import CrawlSpider
from datetime import datetime
from scrapy import signals
from scrapy.exceptions import CloseSpider
from scrapy.http import Request
from scrapy.spidermiddlewares.httperror import HttpError
from twisted.internet.error import DNSLookupError,TimeoutError,TCPTimedOutError,ConnectionRefusedError,NoRouteError

class IndexSpider(CrawlSpider,metaclass=ABCMeta):
  """
  Abstract class for Spiders that crawl news website wich provide numerical indexing, i.e. url in the form `domain/seed/[1-9]`  
  """
  
  url_suffix = str

  def __init__(self,path = '', seed = None , daily = False, stop = None, *args, **kwargs):
    """
    Construct new Spider for domain's seed.
    
    :param path: where to store error file and downloaded file (same as in `../JsonExportPipeline.py`)
    :type path: string
    :param seed: seed of the subdomain to crawl
    :type seed: string
    :param stop: day of news publishing when to stop the crawl (`YYYY-MM-DD`). Retrieved by method `all_stop_dates`. If `None stop only if no more links.
    :type stop: string or None
    :param daily: = wheter to run the Spider in `daily mode`. Requested only one page s.t. index = 1
    :type daily: bool
    :url_suffix: ending string in order to form request url for the `index page`. If not necessary, leave empty
    :type url_suffix: string
    
    :rytpe: scrapy.spiders.CrawlSpider
    """
    super().__init__(*args, **kwargs)
    self.path = path
    self.seed = seed
    self.seed_path = seed.replace('/','_') if len(seed.split('/')) > 1 else seed
    self.daily = bool(daily)
    self.stop = None if stop == None else datetime.strptime(str(stop),'%Y-%m-%d').date()
    self.__error = None
  
  @classmethod
  def from_crawler(cls, crawler, *args, **kwargs):
    spider = super(IndexSpider, cls).from_crawler(crawler, *args, **kwargs)
    crawler.signals.connect(spider.spider_opened, signal=signals.spider_opened)
    crawler.signals.connect(spider.spider_closed, signal=signals.spider_closed)
    return spider
  
  def spider_opened(self,spider):
    """
    Creating a file when the Spider is opened for storing url that encounterd an error. Only if not in `daily mode`.
    """

    
    if self.daily == False:
      try:
        self.__error = open('{0}_{1}_{2}.err'.format(os.path.join(self.path,self.name,self.seed_path,self.name), \
                           self.seed_path,datetime.strftime(datetime.now(),"%d-%m-%Y_%H:%M:%S")),'w+')    
      except IOError:
        self.logger.warning('NO DATA WILL BE STORED! You need to create the download folder for the spider and the category before!')
    else:
      pass
  
  def spider_closed(self,spider):
    """
    Closing `error file` if has been previously created.
    """
    
    if self.daily == False:
      self.__error.close()
    else:
      pass
  
    
  def start_requests(self):
    """
    Spider method to initialize starting requests. Using range from 1 to `sys.maxsize` with step = 1. It stops the spider when the range is over. 
    If Spider is in `daily mode` only one request is made s.t. index = 1
    """
    
    if self.daily == False:
      for i in range(1,sys.maxsize):
        yield Request(url = '{0}/{1}{2}{3}'.format(self.start_urls,self.seed,self.url_suffix,i),callback = self.parse_index,
                      errback = self.errback_parse, dont_filter = True) # do not filter since might be needed to crawl more than once
    else:
      yield Request(url = '{0}/{1}{2}{3}'.format(self.start_urls,self.seed,self.url_suffix,1),callback = self.parse_index,
                      errback = self.errback_parse, dont_filter = True) # do not filter since might be needed to crawl more than once

  
  @abstractmethod 
  def get_links(self,response):
    """
    Method that extract links from the `index page`.
    It needs to be defined by actual Spidder class in order for this one to be instantiated. 
    """
    pass
  
  @abstractmethod 
  def all_stop_dates(self,links):
    """
    Method that extract dates from the retrieved links in the `index page`.
    It needs to be defined by actual Spidder class in order for this one to be instantiated. 
    """
    pass

  def parse_index(self,response):
    """
    Method of the Spider to extract all relevant urls from the index pages. The spider stops requesting pages if:
      - there are no more links to retrieve
      - if stop is defined as a date: if all the retrieved urls have been published before the stop date defiend
    """
    
    self.logger.info('Visiting index page : %s',response.url)
    
    links = self.get_links(response)
    
    if not links:
      raise CloseSpider('No more link to follow')
    else:
      if self.stop != None:
        if self.all_stop_dates(links) == True:
          raise CloseSpider('All retrieved links are older than the defined stop date')
        else:
          for link in links:
            yield Request(url = link.url, callback = self.parse_item, errback = self.errback_parse)
      else:
        for link in links:
          yield Request(url = link.url, callback = self.parse_item, errback = self.errback_parse)
  
  @abstractmethod 
  def parse_item(self,response):
    """
    Method that performs the actual scraping of the retrieved links in the `index page`.
    It needs to be defined by actual Spidder class in order for this one to be instantiated. 
    """
    pass
  
  def errback_parse(self,failure):
    """
    Spider method to catch error in requested pages.
    If Spider runs in `daily mode` error and relative url appear in stdout.
    Otherwise are stored in an `error file`.
    """
    
    def print_err_msg(err_msg):
      if self.daily == True:
        self.logger.error(err_msg)
      else:
        self.__error.write(err_msg)

    
    if failure.check(HttpError):
      response = failure.value.response
      err_msg = 'HttpError on\t{0}\n'.format(response.url)
      print_err_msg(err_msg)
      

    elif failure.check(DNSLookupError):
      request = failure.request
      err_msg = 'DNSLookupError on\t{0}\n'.format(request.url)
      print_err_msg(err_msg)
      
    elif failure.check(TimeoutError, TCPTimedOutError):
      request = failure.request
      err_msg = 'TimeoutError on\t{0}\n'.format(request.url)
      print_err_msg(err_msg)
 
      
    elif failure.check(ConnectionRefusedError):
      request = failure.request
      err_msg = 'ConnectionRefusedError on\t{0}\n'.format(request.url)
      print_err_msg(err_msg)

    elif failure.check(NoRouteError):
      request = failure.request
      err_msg = 'NoRouteError on\t{0}\n'.format(request.url)
      print_err_msg(err_msg)