import os
from abc import ABCMeta,abstractmethod
from scrapy.spiders import CrawlSpider
from datetime import datetime,timedelta
from scrapy import signals
from scrapy.http import Request
from scrapy.spidermiddlewares.httperror import HttpError
from twisted.internet.error import DNSLookupError,TimeoutError,TCPTimedOutError,ConnectionRefusedError,NoRouteError

class ArchiveSpider(CrawlSpider,metaclass=ABCMeta):
  """
  Abstract class for Spiders that crawl archive website, i.e. urls in the form : `domain/YYYY-MM-DD`.
  
  """
  
  url_suffix = str
  
  def __init__(self,path = '', start=  None, stop = '2016-01-01', daily = False, *args, **kwargs):
    """
    Construct new Spider for archives. 
    
    :param path: where to store error file and downloaded file (same as in `../JsonExportPipeline.py`)
    :type path: string
    :param start: day of news publishing from which to start the crawl (`YYYY-MM-DD`)
    :type start: string
    :param stop:  day of news publishing when to stop the crawl (`YYYY-MM-DD`)
    :type stop: string
    :param daily:  wheter to run the Spider in `daily mode`. Requested only one page s.t. day = datetime.now()
    :type daily: bool
    :parm url_suffix: ending string in order to form request url for the `index page`. If not necessary, leave empty
    :param url_suffix: string
    
    :rytpe: scrapy.spiders.CrawlSpider
  
  
    """
    super().__init__(*args, **kwargs)
    self.path = path
    self.start = datetime.strptime(str(start),'%Y-%m-%d').date() if start != None else datetime.now().date()
    self.stop = datetime.strptime(str(stop),'%Y-%m-%d').date()  
    self.daily = bool(daily)
    self.__error = None
  
  @classmethod
  def from_crawler(cls, crawler, *args, **kwargs):
    spider = super(ArchiveSpider, cls).from_crawler(crawler, *args, **kwargs)
    crawler.signals.connect(spider.spider_opened, signal=signals.spider_opened)
    crawler.signals.connect(spider.spider_closed, signal=signals.spider_closed)
    return spider
  
  def spider_opened(self,spider):
    """
    Creating a file when the Spider is opened for storing url that encounterd an error. Only if not in `daily mode`.
    """
    
    if self.daily == False:
    
      try:
        self.__error = open('{0}_{1}.err'.format(os.path.join(self.path,self.name,self.name), \
                           datetime.strftime(datetime.now(),"%d-%m-%Y_%H:%M:%S")),'w+')    
      except IOError:
        self.logger.warning('NO DATA WILL BE STORED! You need to create the download folder for the spider before!')
    
    else:
      pass
    
  def spider_closed(self,spider):
    """
    Closing `error file` if has been previously created.
    """
    
    if self.daily == False:
      self.__error.close()
    else:
      pass
    
  def start_requests(self):
    """
    Spider method to initialize starting requests. In this case range of dates. It stops the spider when the range is over. Crawling backward in time.
    If in `daily mode` requested only one page s.t. day = today
    """
    if self.daily == False:
      for day in range( int((self.start-self.stop).days)+1):
        yield Request(url = '{0}/archive/{1}{2}'.format(self.start_urls,self.start-timedelta(day),self.url_suffix),
                      callback = self.parse_index, errback = self.errback_parse, dont_filter = True)
    else:
      previous_day = datetime.strftime(datetime.now().date() -timedelta(1),"%Y-%m-%d")
      yield Request(url = '{0}/archive/{1}'.format(self.start_urls,previous_day,self.url_suffix),
                      callback = self.parse_index, errback = self.errback_parse,dont_filter = True)
#    
  @abstractmethod 
  def get_links(self,response):
    """
    Method that extract links from the `index page`.
    It needs to be defined by actual Spidder class in order for this one to be instantiated. 
    """
    pass 
      
      
  def parse_index(self, response):
    """
    Method of the Spider to extract all relevant urls from the index pages.
    If in an index page no links are found the page is skipped from actual scraping.
    """
    self.logger.info('Visiting index page : %s',response.url)
    
    links = self.get_links(response)
    
    if links:
      for link in links:
        yield Request(url = link.url, callback = self.parse_item, errback = self.errback_parse)
    else:
      self.logger.info("No link to retrieve for this day %s" %response.url)
  
  @abstractmethod 
  def parse_item(self,response):
    """
    Method that performs the actual scraping of the retrieved links in the `index page`.
    It needs to be defined by actual Spidder class in order for this one to be instantiated. 
    """
    pass
  
  def errback_parse(self,failure):
    """
    Spider method to catch error in requested pages.
    If Spider runs in `daily mode` error and relative url appear in stdout.
    Otherwise are stored in an `error file`.
    """
    
    def print_err_msg(err_msg):
      if self.daily == True:
        self.logger.error(err_msg)
      else:
        self.__error.write(err_msg)

    
    if failure.check(HttpError):
      response = failure.value.response
      err_msg = 'HttpError on\t{0}\n'.format(response.url)
      print_err_msg(err_msg)

    elif failure.check(DNSLookupError):
      request = failure.request
      err_msg = 'DNSLookupError on\t{0}\n'.format(request.url)
      print_err_msg(err_msg)
      
    elif failure.check(TimeoutError, TCPTimedOutError):
      request = failure.request
      err_msg = 'TimeoutError on\t{0}\n'.format(request.url)
      print_err_msg(err_msg)
 
      
    elif failure.check(ConnectionRefusedError):
      request = failure.request
      err_msg = 'ConnectionRefusedError on\t{0}\n'.format(request.url)
      print_err_msg(err_msg)

    elif failure.check(NoRouteError):
      request = failure.request
      err_msg = 'NoRouteError on\t{0}\n'.format(request.url)
      print_err_msg(err_msg)

        
      