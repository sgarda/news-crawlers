#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 19 11:51:57 2017

@author: Samuele Garda
"""
import re
from datetime import datetime
from scrapy.linkextractors.lxmlhtml import LxmlLinkExtractor
from newspaper_crawl.items import ArticleItem
from scrapy.loader import ItemLoader
from scrapy.loader.processors import Compose, Join
from newspaper_crawl.spiders.AbstractSpiders.IndexSpider import IndexSpider

class GuardianSpider(IndexSpider):
  """
  Class that implements the IndexSpider for the Guardian news.
 
  """
  name = "Guardian"
  allowed_domains = ["www.theguardian.com"]
  start_urls = 'https://www.theguardian.com'
  url_suffix = '?page='

  def get_links(self,response):
    """
    Retrieve links from `ìndex page` (e.g. links present in /domain/seed/page=32).
    :return: list (of Link objects)
    """

    links = LxmlLinkExtractor(restrict_xpaths = ['//h1[@class="fc-item__title"]/a',
                                                       '//h2[@class="fc-item__title"]/a',
                                                       '//h2[@class="fc-item__title fc-item__title--quoted"]/a'],
                                    unique = True).extract_links(response)
    return links
    
    
  def all_stop_dates(self,links):
    """
    Define regular expression in order to get dates from links' urls. Then convert in datetime.date for comparison
    :return: True if all dates are before the stop date, else False
    """
    raw_dates = [d[0] for d in [re.findall('\d{4}.[a-z]{3}.\d{2}',link.url) for link in links] if d]
    dates = [datetime.strptime(d,'%Y/%b/%d').date() for d in raw_dates]
    tostop = all(d < self.stop for d in dates)
    return tostop
    
  def parse_item(self, response):
    """
    Spider method that do the actual parsing for the relevant urls.
    """
    self.logger.info('Scraping extracetd link: %s',response.url)
              
    g = ItemLoader(item=ArticleItem(), response = response)
    
    g.default_input_processor = Join(separator='') 
    g.default_output_processor = Compose(lambda x : x[0])
        
    g.add_value('url', response.url)
    
    g.add_xpath('title','//title//text()')
    
    g.add_xpath('subtitle','//meta[@name="description"]/@content')
    
    g.add_xpath('body','//div[@itemprop="articleBody"]//text()')
    
    g.add_xpath('publication_date','//meta[@property="article:published_time"]/@content',
                re = '\d{4}.\d{2}.\d{2}.\d{2}.\d{2}')
    
    g.add_value('category', self.seed)
    
    g.add_xpath('author','//meta[@name="author"]/@content',Join(','))
      
    g.add_xpath('keywords','//meta[@name="keywords"]/@content')
    
    g.add_xpath('news_keywords','//meta[@name="news_keywords"]/@content')
    
    g.add_xpath('article_tag','//meta[@property="article:tag"]/@content')
    
    g.add_xpath('list_of_tags','//a[@class="list-of-tags__item-link"]')
    
#    g.add_xpath('premium','//meta[@name="tmg.premium.state"]/@content')
    
    g.add_value('taxonomy', response.url, re = '/politics/|/environment/|/uk/northenireland/|/business/|/uk/wales/| \
                |/uk/scotland/|/law/|/society/|/education/|/world/|/uk-news/|/commentisfree/|/money/|')
    
    g.add_value('opinion', response.url , re = 'commentisfree')
    
    return g.load_item()    
      
     
    

        
