#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 17 14:49:52 2017

@author: Samuele Garda
"""
from scrapy.linkextractors.lxmlhtml import LxmlLinkExtractor
from newspaper_crawl.items import ArticleItem
from scrapy.loader import ItemLoader
from scrapy.loader.processors import Join,Compose, MapCompose
from newspaper_crawl.spiders.AbstractSpiders.ArchiveSpider import ArchiveSpider
from datetime import datetime

class IndependentArchiveSpider(ArchiveSpider):
  """
  Class that implements the ArchiveSpider for the Independent news archive.
  """
  name = "IndependentArchive"
  allowed_domains = ["www.independent.co.uk"]
  start_urls = "http://www.independent.co.uk"
  url_suffix = ''
  
  def get_links(self, response):
    """
    Retrieve links from `ìndex page` (e.g. links present in /domain/archive/2017-01-01)
    :return: list (of Link objects)
    """
    
    links = LxmlLinkExtractor(allow=[r'/news/uk/',r'news/uk/politics/',r'/news/world/',r'/news/business/'],
                              restrict_xpaths = ['//ol[@class="archive margin"]'],
                              deny_domains = ['news.thestreet.com'],
                              unique = True).extract_links(response)
        
    return links
  
  def normalize_category(self,cat):
    if cat == '/news/uk/politics/':
      category = 'politics'
    elif cat.startswith('/news/uk/'):
      category = 'uk'
    else:
      category = cat.replace('/','').replace('news','')
    
    return category
      
    
  def parse_item(self,response):
    """
    Spider method that do the actual parsing for the relevant urls.
    """
    
    self.logger.info('Scraping extracetd link: %s',response.url)
    
    i = ItemLoader(item=ArticleItem(), response=response)
    
    i.default_input_processor = Join(separator=' ')
    i.default_output_processor = Compose(lambda x : x[0])
    
    i.add_value('url',response.url)  
    
    i.add_xpath('title', '//title//text()', MapCompose(lambda x : x.strip("| The Independent independent_brand_ident_LOGO Untitled"))) 
    
    i.add_xpath('subtitle', '//div[@class="intro"]//text()[normalize-space()]')
    
    # avoid image cations inside body
    i.add_xpath('body','//div[@class="body-content"]/p/text()')
  
    

#'//div[@itemprop="articleBody"]//text()[not(ancestor::ul[@class="legends"] or \
#                ancestor::script or ancestor::style or ancestor::ul[@class="inline-pipes-list"])][normalize-space()]',
#                MapCompose(lambda x : x.replace('Reuse content',''))

    i.add_xpath('publication_date','//li[@class="publish-date"]/amp-timeago/text()',
                MapCompose(lambda x :  datetime.strptime(x.strip(),'%A %d %B %Y %H:%M').strftime('%Y-%m-%d %H:%M')))
                
#                '//div[@itemprop="articleBody"]//meta[@itemprop="datePublished"]/@content',
#                re = '\d{4}.\d{2}.\d{2}.\d{2}.\d{2}')
    
    i.add_value('category',response.url,MapCompose(self.normalize_category), re = '/news/uk/|/news/uk/politics/|/news/world/|/news/business/')
                
    i.add_xpath('author', '//meta[@name="article:author_name"]/@content') 
    
    i.add_xpath('keywords', '//meta[@name="keywords"]/@content') 
    
#    i.add_xpath('news_keywords', '//meta[@name="news_keywords"]/@content' )
    
    
    i.add_xpath('article_tag','//meta[@property="article:tag"]/@content',Join(',') ) 
    
    i.add_xpath('list_of_tags', '//a[@itemprop="keywords"]//text()', Join(','))
    
    i.add_xpath('premium','//meta[@name="tmg.premium.state"]/@content')
    
    i.add_value('taxonomy', response.url, re = '/uk/|/politics/|/world/|/business/')
    
    i.add_value('opinion', response.url , re = 'commentisfree')
   
    return i.load_item()
