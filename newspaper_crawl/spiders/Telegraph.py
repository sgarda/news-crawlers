# #!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  9 14:57:39 2017

@author: Samuele Garda
"""
import re
from datetime import datetime
from scrapy.linkextractors.lxmlhtml import LxmlLinkExtractor
from newspaper_crawl.items import ArticleItem
from scrapy.loader import ItemLoader
from scrapy.loader.processors import Compose,Join
from newspaper_crawl.spiders.AbstractSpiders.IndexSpider import IndexSpider

class TelegraphSpider(IndexSpider):
  """
  Class that implements the IndexSpider for the Guardian news.
  """
  name = "Telegraph"    
  allowed_domains = ["www.telegraph.co.uk"]
  start_urls = 'http://www.telegraph.co.uk'
  url_suffix = '/page-'
  
  def get_links(self, response):
    """
    Retrieve links from `ìndex page` (e.g. links present in /domain/seed/page=32).
    :return: list (of Link objects)
    """
    
    links = None
    
    if self.seed == 'news' and not self.daily: 
      
      links = LxmlLinkExtractor(allow = [r'\d{4}.\d{2}.\d{2}'],
                                restrict_xpaths = "(//ol[@class='js-list-of-entities__container list-of-entities__container'])[6]").extract_links(response)
      
    else:
            
      links = LxmlLinkExtractor(allow = [r'\d{4}.\d{2}.\d{2}'],
                                restrict_xpaths = "(//ol[@class='js-list-of-entities__container list-of-entities__container'])").extract_links(response)
   
    return links
    
  
  def all_stop_dates(self,links):
    """
    Define regular expression in order to get dates from links' urls. Then convert in datetime.date for comparison
    :return: True if all dates are before the stop date, else False
    """
    raw_dates = [d[0] for d in [re.findall('\d{4}/\d{2}/\d{2}',link.url) for link in links] if d]
    dates = [datetime.strptime(d,'%Y/%m/%d').date() for d in raw_dates]
    tostop = all(d < self.stop for d in dates)
    return tostop
        

  def parse_item(self, response):
    """
    Spider method that do the actual parsing for the relevant urls.
    """
    
    self.logger.info('Scraping extracetd link: %s',response.url)

    t = ItemLoader(item=ArticleItem(), response=response)
    
    t.default_input_processor = Join(separator=' ')
    t.default_output_processor = Compose(lambda x : x[0])
    
    t.add_value('url',response.url)
    
    t.add_xpath('title', '//title//text()')
    
    t.add_xpath('subtitle', '//meta[@name="description"]/@content')
    
    t.add_xpath('body', '//article[@itemprop="articleBody"]/descendant-or-self::*[not(self::script or self::style)]/text()[normalize-space()]',Join(separator=''))
    
    t.add_xpath('publication_date', '//meta[@name="DCSext.articleFirstPublished"]/@content',
                re = '\d{4}.\d{2}.\d{2}.\d{2}.\d{2}')
    
    t.add_value('category', self.seed)
                
    t.add_xpath('author', '//meta[@name="DCSext.author"]/@content') 
    
    t.add_xpath('keywords', '//meta[@name="keywords"]/@content') 
    
    t.add_xpath('news_keywords', '//meta[@name="tmgads.keywords"]/@content')
    
    t.add_xpath('article_tag', '//meta[@property="article:tag"]/@content') 
    
    t.add_xpath('list_of_tags', '//a[@class="list-of-tags__link"]//text()',Join(','))
    
    t.add_xpath('premium','//meta[@name="tmg.premium.state"]/@content')
    
    t.add_value('taxonomy', response.url, re = '/news/|/news/uk/|/news/politics/|/education/|/scotland/| \
                /business/|/money/|/politics/|/environment/')
    
#    t.add_value('opinion', response.url , re = 'commentisfree')
   
    return t.load_item()
      