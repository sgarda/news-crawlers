# #!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  9 14:57:39 2017

@author: Samuele Garda
"""

from scrapy.exceptions import DropItem
from datetime import datetime

class CleanArticlePipeline(object):
  """
  Formatting of the parsed item and a couple of basic sanity check.
  Articles with no body are dropped and Warning is raised. 
  """
  
  def process_item(self, item, spider):

    # check for presence of body in article
    try: 
      if item['body']:
        item['body'] = item['body'].replace('\n','').replace('\r','')
    except KeyError:
      raise DropItem("Mising article body in `%s`" %item['url'])
      
    # adjust author field if more than one
    try:
      if item['author']:
        if spider.name == "DailyMail" or spider.name == "TelegraphArchive":
          item['author'] = item['author'].replace('By','').split('and')
        elif spider.name == "Telegraph":
          item['author'] = item['author'].split(';')
        else:
          item['author'] = item['author'].split(',')
      if len(item['author']) > 1:
        item['author'] = [aut.strip() for aut in item['author']]
    except KeyError:
      item['author'] = None
      spider.logger.debug('No author found in this link `%s`. Setted default value `None`' %item['url'])
    
    try:
      if item['publication_date']:
        try:
          if spider.name == "RSSFeedBBC":
            item['publication_date'] = datetime.strptime(item['publication_date'], '%d %B %Y')
          else:
            item['publication_date'] = datetime.strptime(item['publication_date'].replace('T',' '), '%Y-%m-%d %H:%S')
        except ValueError:
          pass
    except KeyError:
      item['publication_date'] = datetime.strptime('1900-01-01','%Y-%m-%d')
      spider.logger.debug('No publication date found in this link `%s`. Setted default value `None`' %item['url'])
    
    try:
      if item['article_tag']:
        item['article_tag'] = item['article_tag'].split(',')
    except KeyError:
      item['article_tag'] = None
      spider.logger.debug('No article tag found in this link `%s`. Setted default value `None`' %item['url'])
    
    try:
      if item['list_of_tags']:
        item['list_of_tags'] = item['list_of_tags'].split(',')
    except KeyError:
      item['list_of_tags'] = None
      spider.logger.debug('No list of tag found in this link `%s`. Setted default value `None`' %item['url'])
    
    try:
      if item['keywords']:
        item['keywords'] = item['keywords'].split(',')
    except KeyError:
      item['keywords'] = None
      spider.logger.debug('No keywords found in this link `%s`. Setted default value `None`' %item['url'])
    
    try:
      if item['news_keywords']:
        item['news_keywords'] = item['news_keywords'].split(',')
    except KeyError:
      item['news_keywords'] = None
      spider.logger.debug('No news keywords found in this link `%s`. Setted default value `None`' %item['url'])
    
    try:
      if item['premium']:
        item['premium'] = True if item['premium'] == 'true' else False
    except KeyError:
      item['premium'] = False
      spider.logger.debug('No premium information found in this link `%s`. Setted default value `False`' %item['url'])
      
    try:
      if item['taxonomy']:
        item['taxonomy'] = item['taxonomy'].lower().replace('/','').replace('. ','')
    except KeyError:
      item['taxonomy'] = ''.join(item['url'].split('/')[3])
      spider.logger.debug('None of defined categories found in this url `%s`. Setted default value: 4th token' %item['url'])
            
    try:
      if item['opinion']:
        item['opinion'] = True
    except KeyError:
      item['opinion'] = False
    
        
    return item
