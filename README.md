# Crawl on-line newspapers

Scrapy project for crawling news from the following newspapers: [the Guardian](https://www.theguardian.com/international),[Telegraph](http://www.telegraph.co.uk/),[Telegraph archive](http://www.telegraph.co.uk/archive/),[Independet archive](http://www.independent.co.uk/article-archive),[DailyMail](http://www.dailymail.co.uk/ushome/index.html),[RSS BBC news](http://feeds.bbci.co.uk/news/rss.xml)

## CRAWL

With this script it is possible to crawl newspapers and store the retrived news in a JSON file and in an ElasticSearch instance. Each of the above mentioned newspaper website as its own specific configuration.

Invoke the script with a configuration file as follows: 

	python runcrawl.py -c ./configs/guardian.config
	
If you wish instead to run all the spyders on a daily basis:

    python runcrawl.py -c ./configs/dailycrawl.config

    
The configuration file present the following options:
	
	daily = True/False
If false the script will run a complete crawl on the news website specified by "single_spider" otherwise it will take all the spiders (comma separated) specified in "daily_spiders" and run a crawl only on the news presented in the first page of the newspaper (supposedly today's news). The spiders specified in these two field must be equal to the name of the spider class module defined in /news-crawl/newspaper_crawl/spiders e.g. :
	
	single_spider = Telegraph
	daily_spiders = Guardian,IndependentArchive
If the spider inheriths from IndexSpider (urls in the form domain/seed/number_of_page)  you need to specify the seeds you want the spider to crawl, e.g.
	
	<spider_name>_seeds
Morover it is possible to specify a date for which if all the links retrieved from the index page are older than that date the spider will stop
	
	<spider_name>_index_stop
otherwise the stop is set to None, meaning that the spider will crawl until there are links available. The same is equivalent for RSSFeedSpider except that it does not implement a stop date : it runs only in daily mode.
If the spider instead inherits from ArchiveSpider (urls in the form domain/YYYY-mm-dd) you need to specify:
	
	<spider_name>_archive_stop
which tells the spider when to stop crawling. It is possible to set as well a <spider_name>_archive_start. If not present the spider will start crawling from today's date.

In /news-crawl/newspaper_crawl/settings.py you need to specify a "DOWNLOAD_DIRECTORY" which is the general path were all the output will be stored. The script will then create a folder for each Spider and a subfolder for each seed (if present). It is possible to modify the download folder for each spider setting the parameter. Moreover you can set an elasticsearch index  and the jobdir, the folder were already seen url are saved (only if daily = False), specific for each spider: 
	
	<spider_name>_download
	<spider_name>_es
	<spider_name>_jobdir
Finally if daily = True it is possible to reset the database where the already seen url have been stored by the deltafecth plugin:
	
	deltafetch_reset
This will cause the spider to lose all the information about previous crawls.

In the configuration file you need to specify as well a path to a JSON file that describes the specification for the index of ES where the articles will be stored. 
This is to allow to create an ES index. This operation is performed only if the index does not exist yet.

    es_mappings = ./configs/es_mappings.json

For all general options specific to the crawl see : /news-crawl/newspaper_crawl/settings.py

## ES_HELPER

Script to manage indeces in Elastisearch instance. Available options are : create, delete, index, reindex.
Invoke:
    python es_helpers.py -h

for option specific to each command.

## CREATING NEW SPIDERS

All the spider inherit from the classes specified in `spiders.AbstractSpiders`. 
From this you can choose:
- ArchiveSpider : crawling archive news websites such as [Telegraph archive](http://www.telegraph.co.uk/archive/). I.e. the url format is `/archive/yyyy-mm-dd`
- IndexSpider : crawling normal news websites where the articles are stored by index such as [Telegraph](http://www.telegraph.co.uk/). I.e. the url format is `/news/seed/#page/title`
- RSSFeedSpider : crawling RSSFeed webpages such as [RSS BBC news](http://feeds.bbci.co.uk/news/rss.xml). I.e. the url format is `/news/seed/rss.xml`

All of them are abstract class. I.e. they just implement the spyder logic and cannot perform any actual crawling. Each of these classes as its own specific attributes related to its logic, please take a look to their `__init__` method!

Once you have picked that fits one of these categories you can inherit from one of the above mentioned classes. 

For all of them you need to specify four attributes, such as:

```python
class TelegraphSpider(IndexSpider):
  name = "Telegraph"    
  allowed_domains = ["www.telegraph.co.uk"]
  start_urls = 'http://www.telegraph.co.uk'
  url_suffix = '/page-'
```

and two methods:

	- get_links(self,response) : retrieve links on the page currently crawled
	- parse_item(self,response) : retrieve fields of interest for each link extracted. E.g. the link title  ('//title//text()')



One final remark. If your spider inherits from IndexSpider you need to specify a method `all_stop_dates(self,links)`. This is beacuse for the spider logic it might be necessary to specify a date before which you do not want to crawl anymore. If this is not set **the spider will crawl until there are no more links available**. Therefore be carefull! 

That's it! Now you have your spider ready to go!

## DEPENDECIES

- scrapy >= 1.4.0
- scrapy-deltafetch 
- scrapyelasticsearch >= 0.9.0
- elsticsearch-py >= 5.4.0
