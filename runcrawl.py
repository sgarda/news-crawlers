#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 19 14:03:32 2017

@author: Samuele Garda
"""

import os
import sys
import logging 
import argparse
import requests
import elasticsearch as es
from importlib import import_module
from configparser import ConfigParser
from scrapy.utils.log import configure_logging, _get_handler
from datetime import datetime
from scrapy.utils.project import get_project_settings
from scrapy.crawler import CrawlerRunner
from twisted.internet import reactor, defer

logger = logging.getLogger(__name__)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')


err = logging.StreamHandler(sys.stdout)
err.setLevel(logging.DEBUG)
err.setFormatter(formatter)
logger.addHandler(err)

def parse_arguments():
  """
  Read configuration file.
  """
  
  parser = argparse.ArgumentParser(description='Run spiders for daily crawl. To edit for options: 1) Crawl specs : config file 2) Spider specs : project/setting.py')
  parser.add_argument('-c', '--conf', required=True, help='Configuration file')
  args = parser.parse_args()
  
  configuration = ConfigParser(allow_no_value=True)
  configuration.read(args.conf)
  config = configuration['crawl']
  
  return config

class RunCrawl(object):
  """
  Object to run crawl of scrapy Spiders: daily mode or normal single crawl.
  """
  
  def __init__(self, config, *args, **kwargs):
    """
    Construct new RunCrawl object with configuration file.
    """
    self.config = config
    self.settings = get_project_settings()
    self.daily = config.getboolean('daily',False)
    self.today = datetime.strftime(datetime.now(),"%Y-%m-%d")
    self.reactor = reactor
    self.spider_classes = []
    self.handler = None
    self.es_mappings = open(config.get('es_mappings','es_mappings.json')).read()
    self.es = es.Elasticsearch(['localhost'])
#   self.es = es.Elasticsearch(self.settings.get('ELASTICSEARCH_SERVERS','localhost'), verify_certs=False,use_ssl = True, http_auth= "admin:admin")
    
  
  def get_spider_classes(self):
    """
    Get spider classes from configuration file.
    """
    spiders_module = self.settings.get('NEWSPIDER_MODULE')
  
    spiders_names = self.config.get('daily_spiders').split(',') if self.daily else [self.config.get('single_spider')]      
        
    for spider in spiders_names:
      try:
        spider_name = spider+'Spider'
        spider_class = getattr(import_module('.'.join([spiders_module,spider])), spider_name)
        self.spider_classes.append(spider_class)
      except AttributeError:
        logger.debug("`%s` class not found! Spider name provided in config file must be the same of Spider class in spider folder!" %spider)
        
        
  def check_es_index(self,index):
    """
    Create Elastic Search index if it does not exist yet.
    """
    # default settings of index      
    
    if not self.es.indices.exists(index):
       self.es.indices.create(index = index, body = self.es_mappings)
       
  def update_mode_settings(self,dl_path,es_ind):
    """
    Update settings.py w.r.t. mode. 
      - daily : enable DeltaFetch middleware, reset DeltaFetch database (optional) , just ERROR in stdout
      - crawl : enable DumpStats extension, print crawl stats in stdout every 2h
    """
    
    if self.daily:
      self.settings.update({'DOWNLOAD_DIRECTORY' : dl_path,'LOG_LEVEL' : 'ERROR','ELASTICSEARCH_INDEX' : es_ind,
      'SPIDER_MIDDLEWARES' : {'scrapy_deltafetch.DeltaFetch': 100},'DELTAFETCH_ENABLED' : True,'DELTAFETCH_RESET' : self.config.get('delta_reset',False)})
    else:
      self.settings.update({'DOWNLOAD_DIRECTORY' : dl_path,'ELASTICSEARCH_INDEX' : es_ind,
      'EXTENSIONS' : {'newspaper_crawl.DumpStatsExtension.DumpStatsExtension': 100},'DUMP_STATS_INTERVAL' : 7200})
      
      
  def update_index_settings(self,dl_path,job_dir,spider,seed_path):
    """
    Update settings if spider class inherits from IndexSpider = has attribute `seed`:
      - daily : set database for DeltaFetch (spider name + seed)
      - crawl : set folder for seen url JOBDIR (spider name + seed), set LOG_FILE in folder where output is stored
    """
    if self.daily:
      self.settings.update({'DELTAFETCH_DIR' : '{0}'.format(os.path.join(dl_path,spider.name,seed_path))})
    else:
      self.settings.update({'LOG_FILE' : '{0}_{1}_{2}.log'.format(os.path.join(dl_path,spider.name,seed_path,spider.name),seed_path, \
                               datetime.strftime(datetime.now(),"%d-%m-%Y_%H:%M:%S")),'JOBDIR' : '{0}'.format(os.path.join(job_dir,spider.name,seed_path))})
  
  def update_archive_settings(self,dl_path,job_dir,spider):
    """
    Update settings if spider class inherits from ArchiveSpider = has attribute `start` and `stop`:
      - daily : set database for DeltaFetch (spider name)
      - crawl : set folder for seen url JOBDIR (spider name), set LOG_FILE in folder where output is stored
    """
    if self.daily:
      self.settings.update({'DELTAFETCH_DIR' : '{0}'.format(os.path.join(dl_path,spider.name))})
    else:
      self.settings.update({'LOG_FILE' : '{0}_{1}.log'.format(os.path.join(dl_path,spider.name,spider.name),datetime.strftime(datetime.now(),"%d-%m-%Y_%H:%M:%S")),
          'JOBDIR' : '{0}'.format(os.path.join(job_dir,spider.name)),
          })      
      
  def _configure_logging(self):
    """
    Set as main logger the current spider.
    """
    configure_logging(settings=self.settings, install_root_handler=False)
    logging.root.setLevel(logging.NOTSET)
    self.handler = _get_handler(self.settings)
    logging.root.addHandler(self.handler)
    
  def print_daily(self, es,index):
    logger.debug("Current number of documents in `{0}` : {1}".format(index, es.count(index = index)['count']))
    
  def print_spec(self,spider_name, dl, job_dir, start, stop, seed = None):
    """
    Print specificatoins of the crawl: spider name, output dir, start date, stop date. Only in `crawl` mode
    """
    if seed == None:
      logger.debug("{0} specifications:\ndonwload = {1}\njobdir = {2}\nstart = {3}\nstop = {4}".format(spider_name,dl,job_dir,start,stop))
    else:
      logger.debug("{0} specifications:\ndonwload = {1}\njobdir = {2}\nstart = {3}\nstop = {4}\nseed={5}".format(spider_name,dl,job_dir,start,stop,seed))
        
  def run_reactor(self):
    
    self.reactor.run()
       
  @defer.inlineCallbacks
  def crawl(self):
    """
    Do crawling deferred (one spider at time). Each Spider (and for each seed if the Spider supports them) is instantiated and the crawl is started.
    It stops once the spider (or all the spiders if in `daily` mode) and all the seeds for the Spider have been crawled
    """
    
    self.get_spider_classes()
        
    # global action for spider
    
    for spider in self.spider_classes:
            
      norm_name = spider.name.replace('Archive','').lower()
      
      store_path = self.config.get('{}_download'.format(norm_name),self.config.get('all_download',self.settings.get('DOWNLOAD_DIRECTORY')))
            
      es_index = self.config.get('{}_es'.format(norm_name),'new_{}'.format(norm_name))
      
      jobdir_path = self.config.get('{}_jobdir'.format(norm_name), self.config.get('all_download',self.settings.get('DOWNLOAD_DIRECTORY')))
                  
      # create folder for storing output
      if not os.path.exists(os.path.join(store_path,spider.name)):
        os.mkdir(os.path.join(store_path,spider.name))
              
      self.check_es_index(es_index)
      
      self.update_mode_settings(store_path,es_index)
    
      seeds = self.config.get('{}_seeds'.format(norm_name),None)
      
      runner = CrawlerRunner(self.settings)
      
      # spiders with `seed` attribute
      if seeds != None: 
        
        seeds = seeds.split(',')
        
        # create subfolder for each seed to store: deltafetch dir if daily else jobdir,logfile
        for seed in seeds:
      
          seed_path = seed.replace('/','_') if len(seed.split('/')) > 1 else seed
          
          if not os.path.exists(os.path.join(store_path,spider.name,seed_path)):
            os.mkdir(os.path.join(store_path,spider.name,seed_path))
            
          self.update_index_settings(store_path,jobdir_path,spider,seed_path)
          
          self._configure_logging()
          
          if self.daily:
            yield runner.crawl(spider,path = store_path, seed=seed,daily = True)
          else:
            stop_index = self.config.get('{}_index_stop'.format(norm_name),None)
            self.print_spec(spider.name, store_path,jobdir_path,self.today,stop_index,seed)
            yield runner.crawl(spider,path = store_path, seed = seed, stop = stop_index,daily=False)
          
          logging.root.removeHandler(self.handler)
      
      # spiders with `start`,`stop` attribute (archives) : single folder for deltafetch dir/jobdir,logfile
      else:
        
        self.update_archive_settings(store_path,jobdir_path,spider)
        
        self._configure_logging()
        
        if self.daily:
          yield runner.crawl(spider,path = store_path, daily = True)
        else:
          start_archive = self.config.get('{}_archive_start'.format(norm_name),None)
          stop_archive = self.config.get('{}_archive_stop'.format(norm_name),'2016-01-01')
          self.print_spec(spider.name, store_path,jobdir_path,start_archive,stop_archive)
          yield runner.crawl(spider,path = store_path, start = start_archive, stop = stop_archive,daily = False) 
          
        logging.root.removeHandler(self.handler)
        
      if self.daily:
        self.print_daily(self.es, es_index)

    self.reactor.stop() 
          
if __name__ == "__main__":
  
  es.connection.http_urllib3.warnings.filterwarnings('ignore')
  requests.packages.urllib3.disable_warnings()
  
  conf = parse_arguments()
  runcrawler = RunCrawl(config= conf)
  runcrawler.crawl()
  runcrawler.run_reactor()
  
  
