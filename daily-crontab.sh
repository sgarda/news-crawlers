#!/bin/bash    

cd ./news-crawlers
pwd
sed -i 's/daily = False/daily = True/g' ./configs/dailycrawl.config
../anaconda3/envs/generalenv/bin/python --version
../anaconda3/envs/generalenv/bin/python ./runcrawl.py -c ./configs/dailycrawl.config


